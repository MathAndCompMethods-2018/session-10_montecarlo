# -*- coding: utf-8 -*-
"""
Created on Wed Jul 20 12:26:03 2016

@author: mnaylor
"""
from matplotlib import pyplot as plt
from scipy import optimize
import numpy as np


aL = np.array( [1.9245108, -0.62385555, -0.32731127, 0.39245142 ])
tL = np.array( [0.34, 0.5, 10./6., 11./6. ])
 
aV = np.array( [  -1.7074879, -0.82274670, -4.6008549, -10.111178, -29.742252 ] )
tV = np.array( [0.34, 0.5 ,1. ,7./3. ,14./3. ] )  

aSpan = np.array( [-7.0602087, 1.9391218, -1.6463597, -3.2995634] )
tSpan = np.array( [1., 1.5, 2., 4.] )

Ci = np.array( [ 0, .376194, .118836, -3.04379, 2.27453, -1.23863, .250442, -.115350, .675104, \
.198861, .216124, -.583148, .0119747, .0537278, .0265216, -2.79498, 5.62393, -2.93831, \
.988759, -3.04711, 2.32316, 1.07379, -.0000599724, .0000885339, .00316418, 10, 50, 80000] )

Rho_c = 10.634        # [kg.mol/m**3]
T_c = 304.19        # [K]
P_c = 7.38        # [MPa]
M_CO2 = 44.0095 ## Molecular weight or molar mass [g/mol] 
    
GasConst= 8.314472e-3
p_c = 7.3773   

def CO2_getDensityFromPT (P, T):   
    ## Critical point
    
    #T = T + 273.15    # [K]
       
    phaseChangePressure = CO2_getPhaseChangePressure(T, P)
    
    satTempAtThisP = CO2_getTempFromVapPressure( P )
    satVapourDensity = CO2_getSatVapDensity( satTempAtThisP ) # * 44.010
    dewPointDensity  = CO2_getDewPointDensity( satTempAtThisP ) #* 44.010
       
       ## Set density range to ensure correct solution is found
    if ( P < phaseChangePressure):
        densityRange = np.array( [0.01, satVapourDensity[0] ])
    if ( P >= phaseChangePressure):     
        densityRange = np.array( [ dewPointDensity[0],22] )
    if ( P > P_c ):               
        densityRange = np.array( [0.01,22] )

    temp = CO2_getDewPointDensity(T)
    if ( (P > P_c) and (T < T_c) ):
        densityRange = np.array( [ temp[0],22 ] ) 
    
    answer = optimize.fminbound(CO2_getHuangDensityDifference,x1=densityRange[0], x2=densityRange[1], args=(P, T))
    density = answer * M_CO2   
    return density


###################################################
###################################################
###################################################
###  Vapour pressure curve like calculations


def CO2_getPhaseChangePressure (T, P):
    ## Might have to include case of no phase change
    phaseChangePressure = 99999

    Pvp = CO2_getVapPressureFromTemp( T )  # where Pvp in MPa, T in K
    if( (Pvp < P) and  (Pvp >= 0) ): 
        phaseChangePressure = Pvp
    return phaseChangePressure

######################################################
## Function to return the vapour pressure from some temp for CO2
# DONE - CHECKED

def CO2_getVapPressureFromTemp (Temp):
     aSpan = np.array([-7.0602087, 1.9391218, -1.6463597, -3.2995634])
     tSpan = np.array([1., 1.5, 2., 4.])

     if(Temp <= T_c):
         tempTermSum = np.sum( aSpan*(1-Temp/T_c)**tSpan)
         ln_Ps_Pc = tempTermSum * T_c / Temp 
         Pvp = p_c * np.exp( ln_Ps_Pc ) 
     else:
         Pvp = -999

     return Pvp
 

######################################################

## Function to return the temp corresponding to some vapour pressure

def CO2_getTempFromVapPressure(P):   
    minTemp = 155.
    maxTemp = 314.15
    ANS=0
    
    pressure = P    
    
    def f(x):
        return (sum( aSpan*(1-x/T_c)**tSpan) * T_c / x - np.log(pressure / p_c))**2
    
    if(P <= p_c):
         Temp = optimize.fminbound(f, x1=minTemp, x2=maxTemp)
    else: 
         ANS = 999 
    
        ## Correct supercritical values for vector input
    if ( P > p_c ):
         T = 999
    
    if(ANS < 999):
         T = Temp
    else:
         T = ANS

    return T


###################################################

def CO2_getDewPointDensity ( T ):  
    satLiqDensity =  Rho_c * np.exp( np.sum(aL*(1-T/T_c)**tL ) )         #density
    satLiqPressure =  CO2_getVapPressureFromTemp( T )
 
    return satLiqDensity, satLiqPressure
 
################################################## 
 
def CO2_getDewPointTemp(density): 

    def f(localT):
        return ( 44.01* Rho_c * np.exp( np.sum(aL*(1-localT/T_c)**tL ) ) - density )**2
    
    dewPointTemp = optimize.fminbound ( f , x1=270., x2=304. )         #density
    return dewPointTemp    

##################################################
# DONE

def CO2_getSatVapDensity ( T ):
    satVapDensity =  Rho_c * np.exp( np.sum( aV*(1-T/T_c)**tV ) )         #density
    satVapPress =  CO2_getVapPressureFromTemp( T )                 #pressure

    return satVapDensity, satVapPress


#################################################

def CO2_getSatVapTemp (density):

    def f(Temp):
        return (44.01 * Rho_c * np.exp( np.sum( aV * (1-Temp/T_c)**tV ) ) - density)**2

    satVapTemp = optimize.fminbound ( f , x1=270, x2=320 )         #density
    return satVapTemp




#################################################
#################################################
##   Plotting

def plotSaturatedDensityCurves():
    plt.figure('satDensityCurves')
    plt.xlabel('Density [kg/m^3]')
    plt.ylabel('Pressure [MPa]')
    
    T=np.arange(180., T_c, 1.)
    N = len(T)
    satLiqDensity =  np.zeros(N) 
    satLiqPressure = np.zeros(N)
    satVapDensity =  np.zeros(N)
    satVapPressure = np.zeros(N)
    
    for i in range(N):
        satLiqDensity[i] =  Rho_c * np.exp( np.sum( aL*(1-T[i]/T_c)**tL ) )
        satLiqPressure[i] = CO2_getVapPressureFromTemp( T[i] )
        satVapDensity[i] =  Rho_c * np.exp( np.sum(aV*(1-T[i]/T_c)**tV ) )
        satVapPressure[i] = CO2_getVapPressureFromTemp( T[i] )

    plt.plot(44.01*satLiqDensity,satLiqPressure)
    plt.plot(44.01*satVapDensity,satVapPressure)
    plt.ylim(0.,14.)



#################################################
# DONE - CHECKED

def CO2_plotVapPressureCurve (maxT=320, maxP=15,addDensityContours=True, tempAxis="K",  densityContourIncrement=100):

    T = np.arange(304.1282, 154.1282,  -1.)
    N = len(T)
    Pvp = np.zeros(N)

    for i in range(N):
        Pvp[i] = CO2_getVapPressureFromTemp( T[i] )  # where Pvp in MPa, T in K
        
    plt.figure('VapourPressureCurve')
    plt.ylabel('Pressure [MPa]')
    plt.xlabel( "Temp [K]")
    plt.xlim(270,maxT)
    plt.ylim(0,maxP)


	## Plot vapour-pressure curve
    plt.plot( T, Pvp)
    plt.axvline(x=T_c)
    plt.axhline(y=P_c)


    for j in range(9):    
        density = 100.*(j+1)
        if (density < 44.010 * Rho_c):
           ## Want temp where at getSatVapourTemp(Density)
                 minTemp = CO2_getSatVapTemp( density )
    
        if (density > 44.010 * Rho_c):
                 minTemp = CO2_getDewPointTemp( density )

        T = np.arange(minTemp, 320., 1.)
        N = len(T)
        pressures = np.zeros(N)
                
        for i in range(N):
            pressures[i] = CO2_getHuangPressure(density/44.010 ,T[i])     
        plt.plot(T, pressures, '--')
            



#################################################
##   Huang equations of state
# DONE
def CO2_getHuangDensityDifference (Rho, P, T ):

    b = np.zeros(9)
    
    # turn T to T/T_c
    
    Treal = T
    T = Treal/T_c
    delT = 1-T
    
    b[2] = Ci[1] + Ci[2]/T +Ci[3]/T**2 +Ci[4]/T**3 +Ci[5]/T**4 +Ci[6]/T**5  
    b[3] = Ci[7] + Ci[8]/T +Ci[9]/T**2  
    b[4] = Ci[10] + Ci[11]/T
    b[5] = Ci[12] + Ci[13]/T
    b[6] = Ci[14]/T
    b[7] = Ci[15]/T**3 +Ci[16]/T**4 +Ci[17]/T**5
    b[8] = Ci[18]/T**3 +Ci[19]/T**4 +Ci[20]/T**5
    
    ##  vary density (R)
    delRho = 1-Rho_c/Rho
    
    ## Calc LHS of (1)
    Zlhs = P / (Rho * GasConst * Treal)
    
    ## Calc RHS of (1)
    Zrhs = 1 + b[2] * Rho/Rho_c
    
    Zrhs = Zrhs + b[3] * (Rho/Rho_c)**2
    Zrhs = Zrhs + b[4] * (Rho/Rho_c)**3
    Zrhs = Zrhs + b[5] * (Rho/Rho_c)**4
    Zrhs = Zrhs + b[6] * (Rho/Rho_c)**5
    Zrhs = Zrhs + b[7] * (Rho/Rho_c)**2 * np.exp(-Ci[21] * (Rho/Rho_c)**2)
    Zrhs = Zrhs + b[8] * (Rho/Rho_c)**4 * np.exp(-Ci[21] * (Rho/Rho_c)**2)
    Zrhs = Zrhs + Ci[22] * (Rho/Rho_c) * np.exp(-Ci[27] * delT**2)
    Zrhs = Zrhs + Ci[23] * (delRho / (Rho/Rho_c)) * np.exp(-Ci[25] * delRho**2 - Ci[27] * delT**2)
    Zrhs = Zrhs + Ci[24] * (delRho / (Rho/Rho_c)) * np.exp(-Ci[26] * delRho**2 - Ci[27] * delT**2)
    
    ## Return the difference between the LHS and RHS of Eqn (1)
    output = Zlhs - Zrhs
    
    if (output<0): 
        output = output * -1
    
    return output


def CO2_getHuangPressure (Rho, T ):

    b = np.zeros(9)
    
    # turn T to T/T_c
    
    Treal = T
    T = Treal/T_c
    delT = 1-T
    
    b[2] = Ci[1] + Ci[2]/T +Ci[3]/T**2 +Ci[4]/T**3 +Ci[5]/T**4 +Ci[6]/T**5  
    b[3] = Ci[7] + Ci[8]/T +Ci[9]/T**2  
    b[4] = Ci[10] + Ci[11]/T
    b[5] = Ci[12] + Ci[13]/T
    b[6] = Ci[14]/T
    b[7] = Ci[15]/T**3 +Ci[16]/T**4 +Ci[17]/T**5
    b[8] = Ci[18]/T**3 +Ci[19]/T**4 +Ci[20]/T**5
    
    ##  vary density (R)
    delRho = 1-Rho_c/Rho
    
    ## Calc RHS of (1)
    Zrhs = 1 + b[2] * Rho/Rho_c
    
    Zrhs = Zrhs + b[3] * (Rho/Rho_c)**2
    Zrhs = Zrhs + b[4] * (Rho/Rho_c)**3
    Zrhs = Zrhs + b[5] * (Rho/Rho_c)**4
    Zrhs = Zrhs + b[6] * (Rho/Rho_c)**5
    Zrhs = Zrhs + b[7] * (Rho/Rho_c)**2 * np.exp(-Ci[21] * (Rho/Rho_c)**2)
    Zrhs = Zrhs + b[8] * (Rho/Rho_c)**4 * np.exp(-Ci[21] * (Rho/Rho_c)**2)
    Zrhs = Zrhs + Ci[22] * (Rho/Rho_c) * np.exp(-Ci[27] * delT**2)
    Zrhs = Zrhs + Ci[23] * (delRho / (Rho/Rho_c)) * np.exp(-Ci[25] * delRho**2 - Ci[27] * delT**2)
    Zrhs = Zrhs + Ci[24] * (delRho / (Rho/Rho_c)) * np.exp(-Ci[26] * delRho**2 - Ci[27] * delT**2)
    
    ## Return the difference between the LHS and RHS of Eqn (1)
    P = Zrhs * (Rho * GasConst * Treal)

    return P
